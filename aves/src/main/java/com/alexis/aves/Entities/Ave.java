package com.alexis.aves.Entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "TONT_AVES")
public class Ave {

    @Id
    @GeneratedValue
    @Column(name = "CDAVE")
    private long id;

    @Column(name = "DSNOMBRE_COMUN")
    private String nombreComun;

    @Column(name = "DSNOMBRE_CIENTIFICO")
    private String nombreCientifico;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "TONT_AVES_PAIS", joinColumns = @JoinColumn(name = "CDAVE", referencedColumnName = "CDAVE"),
            inverseJoinColumns = @JoinColumn(name = "CDPAIS", referencedColumnName = "CDPAIS"))
    Set<Pais> paises;

    public Ave() {

    }

    public Ave(String nombreComun, String nombreCientifico, Pais... paises) {
        this.nombreComun = nombreComun;
        this.nombreCientifico = nombreCientifico;
        this.paises = Stream.of(paises).collect(Collectors.toSet());
        this.paises.forEach(pais -> pais.getAves().add(this));
    }

    public long getId() {
        return id;
    }

    public String getNombreComun() {
        return nombreComun;
    }

    public void setNombreComun(String nombreComun) {
        this.nombreComun = nombreComun;
    }

    public String getNombreCientifico() {
        return nombreCientifico;
    }

    public void setNombreCientifico(String nombreCientifico) {
        this.nombreCientifico = nombreCientifico;
    }

    public void setPaises(Set<Pais> paises) {
        this.paises = paises;
    }

    @JsonIgnoreProperties("aves")
    public Set<Pais> getPaises() {
        return paises;
    }
}
