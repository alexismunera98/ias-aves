package com.alexis.aves.Entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "TONT_PAISES")
public class Pais {
    @Id
    @GeneratedValue
    @Column(name = "CDPAIS")
    private long id;

    @Column(name = "DSNOMBRE")
    private String nombre;

    @ManyToOne
    private Zona zona;

    @ManyToMany(mappedBy = "paises")
    Set<Ave> aves = new HashSet<>();

    public Pais() {

    }

    public Pais(String nombre, Zona zona) {
        this.nombre = nombre;
        this.zona = zona;
    }

    public long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Zona getZona() {
        return zona;
    }

    public void setZona(Zona zona) {
        this.zona = zona;
    }

    public Set<Ave> getAves() {
        return aves;
    }

    public void setAves(Set<Ave> aves) {
        this.aves = aves;
    }
}
