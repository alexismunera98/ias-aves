package com.alexis.aves.Entities;

import javax.persistence.*;

@Entity
@Table(name = "TONT_ZONAS")
public class Zona {
    @Id
    @GeneratedValue
    @Column(name = "CDZONA")
    private long id;

    @Column(name = "DSNOMBRE")
    private String nombre;

    public Zona() {

    }

    public Zona(String nombre) {
        this.nombre = nombre;
    }

    public long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
