package com.alexis.aves.Repositories;

import com.alexis.aves.Entities.Pais;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PaisRepository extends CrudRepository<Pais, Long> {
    public List<Pais> findByZona_Id(Long id);
}
