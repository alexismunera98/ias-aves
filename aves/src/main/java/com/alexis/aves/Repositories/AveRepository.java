package com.alexis.aves.Repositories;

import com.alexis.aves.Entities.Ave;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AveRepository extends CrudRepository<Ave, Long> {
    public List<Ave> findByNombreCientificoContainingOrNombreComunContaining(String nombreCientifico, String nombreComun);
}
