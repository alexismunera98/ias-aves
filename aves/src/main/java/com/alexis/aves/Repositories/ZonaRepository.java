package com.alexis.aves.Repositories;

import com.alexis.aves.Entities.Zona;
import org.springframework.data.repository.CrudRepository;

public interface ZonaRepository extends CrudRepository<Zona, Long> {
}
