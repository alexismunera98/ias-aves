package com.alexis.aves;

import com.alexis.aves.Entities.Pais;
import com.alexis.aves.Repositories.AveRepository;
import com.alexis.aves.Repositories.PaisRepository;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class AvesApplication {

    @Autowired
    AveRepository aveRepository;

    @Autowired
    PaisRepository paisRepository;


    public static void main(String[] args) {
        SpringApplication.run(AvesApplication.class, args);
    }

    @PostConstruct
    void init(){
        aveRepository.findAll().forEach(ave -> {
//            Hibernate.initialize(ave.getPaises());
//            ave.getPaises().add(paisRepository.findById(Long.valueOf("29")).get());
        });

    }

}
