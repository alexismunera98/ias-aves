package com.alexis.aves;

import com.alexis.aves.Entities.Ave;
import com.alexis.aves.Entities.Pais;
import com.alexis.aves.Entities.Zona;
import com.alexis.aves.Repositories.AveRepository;
import com.alexis.aves.Repositories.PaisRepository;
import com.alexis.aves.Repositories.ZonaRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Configuration
public class FillDatabase {
    @Bean
    CommandLineRunner initDatabase(ZonaRepository zonaRepository, AveRepository aveRepository, PaisRepository paisRepository){
        return args -> {
            aveRepository.deleteAll();
            paisRepository.deleteAll();
            zonaRepository.deleteAll();

            Zona z1 = new Zona("Z1");
            Zona z3 = new Zona("AA");
            Zona z4 = new Zona("BAY");
            Zona z6 = new Zona("C9");
            zonaRepository.saveAll(new ArrayList<Zona>(Arrays.asList(z1,z3, z4,z6)));
            paisRepository.save(new Pais("Peru", z1));
            paisRepository.save(new Pais("Ecuador", z1));
            paisRepository.save(new Pais("China", z6));
            paisRepository.save(new Pais("Japon", z6));
            paisRepository.save(new Pais("USA", z4));
            paisRepository.save(new Pais("Canada", z4));
            paisRepository.save(new Pais("España", z3));
            paisRepository.save(new Pais("Portugal", z3));
            aveRepository.save(new Ave("Pajaro", "Pajaritus", new Pais("Colombia", z1)));
            aveRepository.save(new Ave("Colibrí", "Trochilidae", new Pais("Mexico", z4)));
        };
    }
}
