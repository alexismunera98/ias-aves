package com.alexis.aves.Controllers;

import com.alexis.aves.Entities.Pais;
import com.alexis.aves.Entities.Zona;
import com.alexis.aves.Repositories.PaisRepository;
import com.alexis.aves.Repositories.ZonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")
public class ZonaController {

    @Autowired
    ZonaRepository zonaRepository;

    @Autowired
    PaisRepository paisRepository;

    @GetMapping("/zonas")
    public List<Zona> getZonaList(){
         List<Zona> zonas = new ArrayList<>();
         zonaRepository.findAll().forEach(zona -> zonas.add(zona));
         return zonas;
    }

    @GetMapping("/zonas/{id}/paises")
    public List<Pais> getPaisesByZona(@PathVariable Long id){
        List<Pais> paises = new ArrayList<>();
        paisRepository.findByZona_Id(id).forEach(pais -> paises.add(pais));
        return paises;
    }
}
