package com.alexis.aves.Controllers;

import com.alexis.aves.Entities.Ave;
import com.alexis.aves.Repositories.AveRepository;
import com.alexis.aves.Repositories.PaisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")
public class AveController {

    @Autowired
    AveRepository aveRepository;

    @Autowired
    PaisRepository paisRepository;

    @GetMapping("/aves")
    public List<Ave> getAllAves(){
        List<Ave> aves = new ArrayList<>();
        aveRepository.findAll().forEach(aves::add);
        return aves;
    }

    @GetMapping("/aves/{nombre}")
    public List<Ave> getAvesByName(@PathVariable String nombre){
        System.out.println(nombre);
        List<Ave> aves = new ArrayList<>();
        aves.addAll(aveRepository.findByNombreCientificoContainingOrNombreComunContaining(nombre, nombre));
        return aves;
    }

    @PostMapping(value = "/aves", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public Ave saveAve(@RequestBody Map<String, Object> json){
        Ave savedAve = aveRepository.save(new Ave((String) json.get("nombreComun"),
                (String) json.get("nombreCientifico"),
                paisRepository.findById(Long.valueOf(String.valueOf(json.get("pais_id")))).get()));
        System.out.println("Se guardo algo");
        System.out.println(savedAve.getNombreCientifico());
        return savedAve;
    }

    @PutMapping("/aves/{id}")
    public ResponseEntity<Ave> updateEmployee(@PathVariable(value = "id") Long aveId,
                                              @RequestBody Ave aveDetails){
        Ave ave = aveRepository.findById(aveId).get();

        ave.setNombreComun(aveDetails.getNombreComun());
        ave.setNombreCientifico(aveDetails.getNombreCientifico());
        final Ave updatedAve = aveRepository.save(ave);
        return ResponseEntity.ok(updatedAve);
    }

    @DeleteMapping("/aves/{id}")
    public ResponseEntity<String> deleteAve(@PathVariable("id") long id){
        aveRepository.deleteById(id);
        return new ResponseEntity<>("Ave borrada", HttpStatus.OK);
    }
}


