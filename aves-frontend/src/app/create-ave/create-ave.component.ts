import {Component, Input, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Zona} from '../zona';
import {ZonasService} from '../zonas.service';
import {AvesListComponent} from '../aves-list/aves-list.component';
import {AvesService} from '../aves.service';

@Component({
  selector: 'create-ave',
  templateUrl: './create-ave.component.html',
  styleUrls: ['./create-ave.component.css']
})
export class CreateAveComponent implements OnInit {

  @Input() listComponent: AvesListComponent;
  nombreComunToCreate: String;
  nombreCientificoToCreate: String;
  zonas: Observable<Zona[]>;
  paises: Observable<any[]>;
  selectedZona: Zona;
  selectedPais: any;

  constructor(private zonaService: ZonasService, private aveService: AvesService) {
  }

  ngOnInit() {
    this.zonaService.getAllZonas().subscribe(value => this.zonas = value);
  }

  cargarPaises() {
    this.zonaService.getPaisesByZona(this.selectedZona.id).subscribe(value => this.paises = value);
  }

  createAve() {
    this.aveService.createAve(this.nombreComunToCreate, this.nombreCientificoToCreate, this.selectedPais.id).subscribe(
      value => {
        console.log(value);
        this.listComponent.reloadData();
      }
    );
  }
}
