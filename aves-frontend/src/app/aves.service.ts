import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Ave} from './ave';

@Injectable({
  providedIn: 'root'
})
export class AvesService {

  constructor(private http: HttpClient) { }

  private baseUrl = 'http://localhost:8080/api/v1';

  deleteAve(id: number): Observable<any>{
    return this.http.delete(`${this.baseUrl}/aves/${id}`);
  }

  createAve(nombreComun :String, nombreCientifico: String, paisId: number): Observable<any>{
    return this.http.post(`${this.baseUrl}/aves`, {"nombreComun": nombreComun, "nombreCientifico": nombreCientifico,
    "pais_id": paisId});
  }

  getAllAves(): Observable<any>{
    return this.http.get(`${this.baseUrl}/aves`);
  }

  updateAve(ave: Ave): Observable<any>{
    return this.http.put(`${this.baseUrl}/aves/${ave.id}`, ave);
  }

  getAvesByName(nombre: String): Observable<any>{
    return this.http.get(`${this.baseUrl}/aves/${nombre}`);
  }
}
