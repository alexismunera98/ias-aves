import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ZonasService {

  private baseUrl = 'http://localhost:8080/api/v1';

  constructor(private http: HttpClient) { }

  getAllZonas(): Observable<any>{
      return this.http.get(`${this.baseUrl}/zonas`);
  }

  getPaisesByZona(id: number): Observable<any>{
    return this.http.get(`${this.baseUrl}/zonas/${id}/paises`);
  }

}
