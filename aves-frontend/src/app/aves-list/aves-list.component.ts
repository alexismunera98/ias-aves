import { Component, OnInit } from '@angular/core';
import {AvesService} from '../aves.service';
import {Observable} from 'rxjs';
import {Ave} from '../ave';
import {Zona} from '../zona';
import {ZonasService} from '../zonas.service';

@Component({
  selector: 'app-aves-list',
  templateUrl: './aves-list.component.html',
  styleUrls: ['./aves-list.component.css']
})
export class AvesListComponent implements OnInit {
  selectedZona: Zona;
  nombreABuscar: String = "Ingrese el ave a buscar";
  aves: Observable<Ave[]>;
  zonas: Observable<Zona[]>;
  constructor(private avesService: AvesService, private zonasService: ZonasService) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData(){
    this.avesService.getAllAves().subscribe(value => this.aves = value);
    this.zonasService.getAllZonas().subscribe(value => this.zonas = value);
  }

  findByName(event: any){
    this.avesService.getAvesByName(event.target.value).subscribe(value => this.aves= value);
  }

  showAve(ave: Ave): boolean{
    if(!this.selectedZona){
      return true;
    }
    let cont = 0;
    ave.paises.forEach(value => {
      if (value.zona.id == this.selectedZona.id){
        cont += 1;
      }
    })
    return cont>0;
  }

  removeFilters(){
    this.selectedZona = undefined;
    this.nombreABuscar = "";
    this.reloadData();
  }

}
