import {Component, Input, OnInit} from '@angular/core';
import {Ave} from '../ave';
import {AvesService} from '../aves.service';
import {AvesListComponent} from '../aves-list/aves-list.component';

@Component({
  selector: 'app-ave-details',
  templateUrl: './ave-details.component.html',
  styleUrls: ['./ave-details.component.css']
})
export class AveDetailsComponent implements OnInit {

  @Input() ave: Ave;

  constructor(private avesService: AvesService, private listAvesComponent: AvesListComponent) { }

  ngOnInit() {
  }

  updateAve(){
    this.avesService.updateAve(this.ave).subscribe(value => {
      console.log(value);
      this.listAvesComponent.reloadData();
    })
  }

  deleteAve(){
    this.avesService.deleteAve(this.ave.id).subscribe(value =>
    {
      console.log(value);
      this.listAvesComponent.reloadData();
    })
  }

}
