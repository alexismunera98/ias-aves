import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AveDetailsComponent } from './ave-details.component';

describe('AveDetailsComponent', () => {
  let component: AveDetailsComponent;
  let fixture: ComponentFixture<AveDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AveDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AveDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
