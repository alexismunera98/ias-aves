import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AvesComponent } from './aves/aves.component';
import { AveDetailsComponent } from './ave-details/ave-details.component';
import { AvesListComponent } from './aves-list/aves-list.component';
import { CreateAveComponent } from './create-ave/create-ave.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    AvesComponent,
    AveDetailsComponent,
    AvesListComponent,
    CreateAveComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
